package edu.rit.cs;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;

import java.io.File;

import static org.apache.spark.sql.functions.lit;
import static org.apache.spark.sql.functions.sum;

/**
 * Class implementing methods for finding Diversity Index.
 *
 */
public class DiversityIndex_Spark
{
    public static final String OutputDirectory = "us_census_diversity_index/dataset/USCD_DI_Output";
    public static final String DatasetFile = "us_census_diversity_index/dataset/cc-est2017-alldata.csv";

    public static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null)
            for (File file : allContents)
                deleteDirectory(file);
        return directoryToBeDeleted.delete();
    }

    static void generateDiversityIndex(SparkSession spark){
        Dataset ds = spark.read()
                .option("header", "true")
                .option("delimiter", ",")
                .option("inferSchema", "true")
                .option("encoding", "ISO-8859-1")
                .csv(DatasetFile);

        ds = ds.select("STNAME", "CTYNAME", "AGEGRP", "TOT_POP", "WA_MALE", "WA_FEMALE", "BA_MALE", "BA_FEMALE",
                "IA_MALE", "IA_FEMALE", "AA_MALE", "AA_FEMALE", "NA_MALE", "NA_FEMALE", "TOM_MALE", "TOM_FEMALE");

        ds = ds.filter(ds.col("AGEGRP").equalTo(0));

        ds = ds.withColumn("WA", ds.col("WA_MALE").plus(ds.col("WA_FEMALE")));
        ds = ds.withColumn("BA", ds.col("BA_MALE").plus(ds.col("BA_FEMALE")));
        ds = ds.withColumn("IA", ds.col("IA_MALE").plus(ds.col("IA_FEMALE")));
        ds = ds.withColumn("AA", ds.col("AA_MALE").plus(ds.col("AA_FEMALE")));
        ds = ds.withColumn("NA", ds.col("NA_MALE").plus(ds.col("NA_FEMALE")));
        ds = ds.withColumn("TOM", ds.col("TOM_MALE").plus(ds.col("TOM_FEMALE")));

        ds = ds.select("STNAME", "CTYNAME", "TOT_POP", "WA", "BA", "IA", "AA", "NA", "TOM");

        ds = ds.groupBy("STNAME", "CTYNAME").agg(sum("TOT_POP").as("AGG_TOT_POP"), sum("WA").as("AGG_WA"),
                sum("BA").as("AGG_BA"), sum("IA").as("AGG_IA"), sum("AA").as("AGG_AA"),
                sum("NA").as("AGG_NA"), sum("TOM").as("AGG_TOM"));

        ds = ds.withColumn("DI", lit(0));
        String[] raceCols = ds.columns();
        for(int i=3; i < raceCols.length; i++){
            Column tempDI = (ds.col(raceCols[i]).multiply(ds.col("AGG_TOT_POP").minus(ds.col(raceCols[i]))))
                            .divide(ds.col("AGG_TOT_POP").multiply(ds.col("AGG_TOT_POP")));
            ds = ds.withColumn("DI", ds.col("DI").plus(tempDI));
        }

        ds = ds.select("STNAME", "CTYNAME", "DI");
        ds = ds.sort("STNAME", "CTYNAME");
        ds.show();

        deleteDirectory(new File(OutputDirectory));
        ds.write().option("header", "true").csv(OutputDirectory);
    }

    public static void main( String[] args )
    {
        // Create a SparkConf that loads defaults from system properties and the classpath
        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.master", "local[4]");

        //Provides the Spark driver application a name for easy identification in the Spark or Yarn UI
        sparkConf.setAppName("Diversity Index");

        // Creating a session to Spark. The session allows the creation of the
        // various data abstractions such as RDDs, DataFrame, and more.
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

        // Creating spark context which allows the communication with worker nodes
        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());

        generateDiversityIndex(spark);

        // Stop existing spark context
        jsc.close();

        // Stop existing spark session
        spark.close();
    }
}
