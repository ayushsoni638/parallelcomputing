package edu.rit.cs;

/**
 * Class Point encapsulates a two-dimensional point.
 */
public class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object other) {
        Point otherPoint = (Point) other;
        return otherPoint.x == this.x && otherPoint.y == this.y;
    }

    @Override
    public int hashCode() {
        return (int) (x + y); // same values should hash to the same number
    }
}