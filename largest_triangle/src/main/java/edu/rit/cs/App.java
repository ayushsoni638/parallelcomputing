package edu.rit.cs;

import java.util.ArrayList;

/**
 * Class implementing the sequential algorithm for finding the largest triangle. QuickHull algorithm code is based on
 * https://www.sanfoundry.com/java-program-implement-quick-hull-algorithm-find-convex-hull/
 * and modified as needed.
 */
public class App {
    public static void main(String args[]) {

//        System.out.println("Finding points forming convex hull...");
        long start = System.currentTimeMillis();

        RandomPoints rndPoints = new RandomPoints(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));
        ArrayList<Point> points = new ArrayList<>();

        // Store all the points in an ArrayList.
        while (rndPoints.hasNext()) {
            points.add(rndPoints.next());
        }

        // run the QuickHull algorithm to find the convex hull of these points.
        ArrayList<Point> hullPoints = quickHull(points);

//        System.out.println("Convex hull points:");
//        for (int i = 0; i < hullPoints.size(); i++)
//            System.out.println(i+": x=" + hullPoints.get(i).getX() + " y=" + hullPoints.get(i).getY());

        int cvHullSize = hullPoints.size();
//        System.out.println("Num of points in convex hull:" + cvHullSize);
        double maxArea = 0;
        Point[] maxAreaPoints = new Point[3];

        // once the points of convex hull are found, brute force on them to find largest triangle.

        for (int i = 0; i < cvHullSize - 2; i++) {
            for (int j = i + 1; j < cvHullSize - 1; j++) {
                for (int k = j + 1; k < cvHullSize; k++) {
                    double thisArea = triangleArea(hullPoints.get(i), hullPoints.get(j), hullPoints.get(k));
                    if (thisArea > maxArea) {
                        maxArea = thisArea;
                        maxAreaPoints[0] = hullPoints.get(i);
                        maxAreaPoints[1] = hullPoints.get(j);
                        maxAreaPoints[2] = hullPoints.get(k);
                    }
                }
            }
        }

        // print the relevant info.
        for (Point p : maxAreaPoints) {
            System.out.printf("%d %.5g %.5g%n", points.indexOf(p) + 1, p.getX(), p.getY());
        }
        System.out.printf("%.5g%n", maxArea);
        System.out.println("Time elapsed: " + (System.currentTimeMillis() - start) + " ms");
    }

    /**
     * The root method to start the QuickHull algorithm recursion.
     *
     * @param points the list of all points.
     * @return the list of points forming convex hull.
     */
    static ArrayList<Point> quickHull(ArrayList<Point> points) {

        ArrayList<Point> convexHull = new ArrayList<>();
        if (points.size() < 3)
            return points;

        // find the leftmost and rightmost points in the set of points, call them A and B.
        int minX_ind = 0, maxX_ind = 0;
        for (int i = 0; i < points.size(); i++) {
            if (points.get(i).getX() < points.get(minX_ind).getX()) {
                minX_ind = i;
            }
            if (points.get(i).getX() > points.get(maxX_ind).getX()) {
                maxX_ind = i;
            }
        }

        Point A = points.get(minX_ind);
        Point B = points.get(maxX_ind);

        // These points always lie on the convex hull, hence add them to its list.
        convexHull.add(A);
        convexHull.add(B);

        ArrayList<Point> leftSubset = new ArrayList<>();
        ArrayList<Point> rightSubset = new ArrayList<>();

        for (Point p : points) {
            if (pointLocation(A, B, p) == -1) // find points to the left of AB
                leftSubset.add(p);
            else if (pointLocation(A, B, p) == 1) // find points to the right of AB
                rightSubset.add(p);
        }

        // since A, B are already in the hull list, don't check them further.
        leftSubset.remove(A);
        leftSubset.remove(B);
        rightSubset.remove(A);
        rightSubset.remove(B);

        // run QuickHull on the points to the left of AB.
        quickHullHelper(A, B, leftSubset, convexHull);

        // run QuickHull on the points to the left of BA.
        quickHullHelper(B, A, rightSubset, convexHull);

        return convexHull;
    }

    /**
     * Main implementation of QuickHull recursive algorithm.
     *
     * @param A
     * @param B
     * @param pointsSubset
     * @param hull
     */
    static void quickHullHelper(Point A, Point B, ArrayList<Point> pointsSubset,
                                ArrayList<Point> hull) {
        if (pointsSubset.size() == 0)
            return;
        if (pointsSubset.size() == 1) {
            Point p = pointsSubset.get(0);
            pointsSubset.remove(p);
            hull.add(p);
            return;
        }

        // find the point farthest and to the left of AB.
        double maxDist = Integer.MIN_VALUE;
        int farthestPointInd = -1;
        for (int i = 0; i < pointsSubset.size(); i++) {
            Point p = pointsSubset.get(i);
            double distance = perpendicularDist(A, B, p);
            if (distance > maxDist) {
                maxDist = distance;
                farthestPointInd = i;
            }
        }

        // This point will also be referred to as P.
        Point farthestPoint = pointsSubset.get(farthestPointInd);

        // once found, add it to convex hull and remove from list of points to be checked further.
        pointsSubset.remove(farthestPointInd);
        hull.add(farthestPoint);

        // Points inside triangle APB, where P is farthestPoint, can never be in the convex hull, so remove them.
        pointsSubset.removeIf(testPoint -> isInsideTriangle(testPoint, A, B, farthestPoint));

        // Determine the points to the left of AP
        ArrayList<Point> leftSetAP = new ArrayList<>();
        for (int i = 0; i < pointsSubset.size(); i++) {
            Point M = pointsSubset.get(i);
            if (pointLocation(A, farthestPoint, M) == -1) {
                leftSetAP.add(M);
            }
        }

        // Determine the points to the left of PB
        ArrayList<Point> leftSetPB = new ArrayList<>();
        for (int i = 0; i < pointsSubset.size(); i++) {
            Point M = pointsSubset.get(i);
            if (pointLocation(farthestPoint, B, M) == -1) {
                leftSetPB.add(M);
            }
        }

        // repeat the recursion on points to the left of AP
        quickHullHelper(A, farthestPoint, leftSetAP, hull);

        // repeat the recursion on points to the left of PB
        quickHullHelper(farthestPoint, B, leftSetPB, hull);
    }

    /**
     * A method to find whether a point P is to the left of, right of, or on the line AB.
     *
     * @param A
     * @param B
     * @param P
     * @return -1 if P is on the left, 0 if on the line, and +1 if P is on the right of AB.
     */
    static int pointLocation(Point A, Point B, Point P) {

        double cartProduct = (B.getX() - A.getX()) * (P.getY() - A.getY()) - (B.getY() - A.getY()) * (P.getX() - A.getX());
        if (cartProduct > 0)
            return -1; // Point to the left of AB
        else if (cartProduct == 0)
            return 0;
        else
            return 1; // Point to the right of AB
    }

    /**
     * A helper method to find the perpendicular distance of point P from line AB.
     *
     * @param A
     * @param B
     * @param P
     * @return
     */
    static double perpendicularDist(Point A, Point B, Point P) {
        double ABx = B.getX() - A.getX();
        double ABy = B.getY() - A.getY();
        double num = ABx * (A.getY() - P.getY()) - ABy * (A.getX() - P.getX());
        if (num < 0)
            num = -num;
        return num;
    }

    /**
     * A method to find if a testPoint is inside a triangle ABC.
     *
     * @param testPoint
     * @param A
     * @param B
     * @param C
     * @return
     */
    static boolean isInsideTriangle(Point testPoint, Point A, Point B, Point C) {
        double as_x = testPoint.getX() - A.getX();
        double as_y = testPoint.getY() - A.getY();

        boolean s_ab = (B.getX() - A.getX()) * as_y - (B.getY() - A.getY()) * as_x > 0;

        if ((C.getX() - A.getX()) * as_y - (C.getY() - A.getY()) * as_x > 0 == s_ab)
            return false;

        if ((C.getX() - B.getX()) * (testPoint.getY() - B.getY()) - (C.getY() - B.getY()) * (testPoint.getX() - B.getX()) > 0 != s_ab)
            return false;

        return true;
    }

    /**
     * A method to calculate the the area of a triangle with given vertices A, B and C.
     *
     * @param A
     * @param B
     * @param C
     * @return
     */
    static double triangleArea(Point A, Point B, Point C) {
        double area = (A.getX() * (B.getY() - C.getY()) + B.getX() * (C.getY() - A.getY()) +
                C.getX() * (A.getY() - B.getY())) / 2.0f;
        return Math.abs(area);
    }
}