package edu.rit.cs;

import mpi.MPI;
import mpi.MPIException;

import java.util.ArrayList;;

/**
 * Class implementing the Distributed algorithm for finding largest triangle. QuickHull algorithm code is based on
 * https://www.sanfoundry.com/java-program-implement-quick-hull-algorithm-find-convex-hull/
 * and is modified for distributed execution.
 */
public class AppMPI {
    public static void main(String args[]) throws MPIException {

        MPI.Init(args);
        int myRank = MPI.COMM_WORLD.getRank(), nWorkers = MPI.COMM_WORLD.getSize();
        long start = 0;
        if (myRank == 0) {
//            System.out.println("Finding points forming convex hull...");
            start = System.currentTimeMillis();
        }

        int numPoints = Integer.parseInt(args[0]);
        ArrayList<Point> globalPoints = null;

        // If this is root node/machine, store a list of ALL the points in this node.
        if (myRank == 0) {
            RandomPoints rndPoints = new RandomPoints(numPoints, Integer.parseInt(args[1]), Integer.parseInt(args[2]));
            globalPoints = new ArrayList<>();
            while (rndPoints.hasNext()) {
                globalPoints.add(rndPoints.next());
            }
        }

        // partition the search space between workers.
        int partitionSize = numPoints / nWorkers;
        RandomPoints rndPoints = new RandomPoints(numPoints, Integer.parseInt(args[1]), Integer.parseInt(args[2]));

        // All the machines will have their own partition of points.
        ArrayList<Point> localPoints = new ArrayList<>();
        ArrayList<Point> convexHull = new ArrayList<>();


        double localMinX_x = Double.MAX_VALUE, localMinX_y = 0,
                localMaxX_x = Double.MIN_VALUE, localMaxX_y = 0;

        // find local points with min and max X values at each machine.
        for (int i = 0; i < partitionSize * (myRank + 1); i++) {
            Point p = rndPoints.next();
            if (i >= partitionSize * (myRank)) {
                localPoints.add(p);
                if (p.getX() < localMinX_x) {
                    localMinX_x = p.getX();
                    localMinX_y = p.getY();
                }
                if (p.getX() > localMaxX_x) {
                    localMaxX_x = p.getX();
                    localMaxX_y = p.getY();
                }
            }
        }

        if (myRank == nWorkers - 1) {
            while (rndPoints.hasNext()) {
                Point p = rndPoints.next();
                localPoints.add(p);
                if (p.getX() < localMinX_x) {
                    localMinX_x = p.getX();
                    localMinX_y = p.getY();
                }
                if (p.getX() > localMaxX_x) {
                    localMaxX_x = p.getX();
                    localMaxX_y = p.getY();
                }
            }
        }

        double[] sendBuf = new double[4];
        double[] recvBuf = new double[4 * nWorkers];
        sendBuf[0] = localMinX_x;
        sendBuf[1] = localMinX_y;
        sendBuf[2] = localMaxX_x;
        sendBuf[3] = localMaxX_y;

        // gather the local min and max points of each machine into root machine(0).
        MPI.COMM_WORLD.gather(sendBuf, 4, MPI.DOUBLE, recvBuf, 4, MPI.DOUBLE, 0);

        // find global min and max extreme points(A and B) based on their X values. This will be done by root(0).
        // Once found, add them to convex hull.
        double[] ABBuf = new double[4];
        if (myRank == 0) {
            int minXInd = 0, maxXInd = 2;
            for (int i = 4; i <= recvBuf.length - 4; i += 4) {
                if (recvBuf[i] < recvBuf[minXInd])
                    minXInd = i;
                if (recvBuf[i + 2] > recvBuf[maxXInd])
                    maxXInd = i;
            }

            ABBuf[0] = recvBuf[minXInd];
            ABBuf[1] = recvBuf[minXInd + 1];
            ABBuf[2] = recvBuf[maxXInd];
            ABBuf[3] = recvBuf[maxXInd + 1];
            convexHull.add(new Point(ABBuf[0], ABBuf[1]));
            convexHull.add(new Point(ABBuf[2], ABBuf[3]));
        }

        // Broadcast these global extreme points to each machine.
        MPI.COMM_WORLD.bcast(ABBuf, 4, MPI.DOUBLE, 0);

        // If this machine had the global min or max points, remove them from its local point list.
        Point A = new Point(ABBuf[0], ABBuf[1]), B = new Point(ABBuf[2], ABBuf[3]);
        int minInd = localPoints.indexOf(A), maxInd = localPoints.indexOf(B);
        if (minInd != -1)
            localPoints.remove(minInd);
        if (maxInd != -1)
            localPoints.remove(maxInd);

        // Run distributed implementation of QuickHull on the points to the left of line AB.
        distributedQuickHull(myRank, nWorkers, ABBuf, localPoints, convexHull);

        // Run distributed implementation of QuickHull on the points to the left of line BA.
        double[] BABuf = {ABBuf[2], ABBuf[3], ABBuf[0], ABBuf[1]};
        distributedQuickHull(myRank, nWorkers, BABuf, localPoints, convexHull);

//        if(myRank == 0) {
//            System.out.println("Points in convex hull:");
//            int idx = 0;
//            for(Point p : convexHull) {
//                System.out.println(idx+": x=" + p.getX() + " y=" + p.getY());
//                idx++;
//            }
//        }

        // If I am root machine, brute force on the convex hull points to find the max area triangle, and print
        // relevant info.
        if (myRank == 0) {
            int cvHullSize = convexHull.size();
//            System.out.println("Num of points in convex hull:" + cvHullSize);
            double maxArea = 0;
            Point[] maxAreaPoints = new Point[3];
            for (int i = 0; i < cvHullSize - 2; i++) {
                for (int j = i + 1; j < cvHullSize - 1; j++) {
                    for (int k = j + 1; k < cvHullSize; k++) {
                        double thisArea = triangleArea(convexHull.get(i), convexHull.get(j), convexHull.get(k));
                        if (thisArea > maxArea) {
                            maxArea = thisArea;
                            maxAreaPoints[0] = convexHull.get(i);
                            maxAreaPoints[1] = convexHull.get(j);
                            maxAreaPoints[2] = convexHull.get(k);
                        }
                    }
                }
            }

            for (Point p : maxAreaPoints) {
                System.out.printf("%d %.5g %.5g%n", globalPoints.indexOf(p) + 1, p.getX(), p.getY());
            }
            System.out.printf("%.5g%n", maxArea);
            System.out.println("Time elapsed: " + (System.currentTimeMillis() - start) + " ms");
        }
        MPI.COMM_WORLD.barrier();
        MPI.Finalize();
    }

    /**
     * The main distributed, recursive QuickHull implementation.
     *
     * @param myRank
     * @param nWorkers
     * @param ABBuf
     * @param localPoints
     * @param convexHull
     * @throws MPIException
     */
    static void distributedQuickHull(int myRank, int nWorkers, double[] ABBuf, ArrayList<Point> localPoints,
                                     ArrayList<Point> convexHull) throws MPIException {

        Point A = new Point(ABBuf[0], ABBuf[1]), B = new Point(ABBuf[2], ABBuf[3]);

        // find the local farthest point to the left of line AB.
        double localMaxDist = Double.MIN_VALUE;
        int localFarthestPointInd = -1;
        for (int i = 0; i < localPoints.size(); i++) {
            Point p = localPoints.get(i);
            if (pointLocation(A, B, p) == -1) {
                double distance = perpendicularDist(A, B, p);
                if (distance > localMaxDist) {
                    localMaxDist = distance;
                    localFarthestPointInd = i;
                }
            }
        }

        double[] sendBuf = new double[2];
        double[] recvBuf = new double[2 * nWorkers];
        if (localFarthestPointInd != -1) {
            Point localFarthestPoint = localPoints.get(localFarthestPointInd);
            sendBuf[0] = localFarthestPoint.getX();
            sendBuf[1] = localFarthestPoint.getY();
        } else {
            sendBuf[0] = -1;
            sendBuf[1] = -1;
        }

        // gather the local farthest points from each machine to root machine.
        MPI.COMM_WORLD.gather(sendBuf, 2, MPI.DOUBLE, recvBuf, 2, MPI.DOUBLE, 0);

        double[] globalFarPointBuf = new double[2];

        // If root, find the global farthest point from the local farthest points of all machines.
        Point globalFarthestPoint = null;
        if (myRank == 0) {
//            System.out.println("recvBuff: " + Arrays.toString(recvBuf));
            int emptyLocalFarPtCount = 0;
            double globalMaxDist = Double.MIN_VALUE;

            for (int i = 0; i <= recvBuf.length - 2; i += 2) {
                if (recvBuf[i] < 0) {
                    emptyLocalFarPtCount++;
                } else {
                    Point p = new Point(recvBuf[i], recvBuf[i + 1]);
                    double dist = perpendicularDist(A, B, p);
                    if (dist > globalMaxDist) {
                        globalMaxDist = dist;
                        globalFarthestPoint = p;
                    }
                }
            }
            if (emptyLocalFarPtCount == nWorkers) {
                // if there was no point to the left of AB on any machine, set global far point to (-1, -1).
                globalFarPointBuf[0] = -1;
                globalFarPointBuf[1] = -1;
//                System.out.println("Terminating...");
            } else {
                // otherwise, if a global farthest point exists, add it to convex hull.
                convexHull.add(globalFarthestPoint);
                globalFarPointBuf[0] = globalFarthestPoint.getX();
                globalFarPointBuf[1] = globalFarthestPoint.getY();
//                System.out.println("Added: "+ Arrays.toString(globalFarPointBuf));
            }
        }

        // broadcast this global farthest point to all machines.
        MPI.COMM_WORLD.bcast(globalFarPointBuf, 2, MPI.DOUBLE, 0);

        // if there was no point to the left of AB on any machine, we have discovered all the points possible on the
        // left side of AB, hence end recursion.
        if (globalFarPointBuf[0] < 0) {
            return;
        }

        if (myRank != 0)
            globalFarthestPoint = new Point(globalFarPointBuf[0], globalFarPointBuf[1]);

        // remove global farthest point from whichever machine it is in.
        if (localFarthestPointInd != -1) {
            int globarFarInd = localPoints.indexOf(globalFarthestPoint);
            if (globarFarInd != -1) {
                localPoints.remove(globarFarInd);
            }
        }

        // Remove all the local points inside triangle APB, where P is the global farthest point.
        Point finalGlobalFarthestPoint = globalFarthestPoint;
        localPoints.removeIf(testPoint -> isInsideTriangle(testPoint, A, B, finalGlobalFarthestPoint));

        // repeat distributed recursion on the points to the left of AP.
        double[] APBuf = {ABBuf[0], ABBuf[1], globalFarPointBuf[0], globalFarPointBuf[1]};
        distributedQuickHull(myRank, nWorkers, APBuf, localPoints, convexHull);

        // repeat distributed recursion on the points to the left of PB.
        double[] PBBuf = {globalFarPointBuf[0], globalFarPointBuf[1], ABBuf[0], ABBuf[1]};
        distributedQuickHull(myRank, nWorkers, PBBuf, localPoints, convexHull);
    }


    /**
     * A method to find whether a point P is to the left of, right of, or on the line AB.
     *
     * @param A
     * @param B
     * @param P
     * @return -1 if P is on the left, 0 if on the line, and +1 if P is on the right of AB.
     */
    static int pointLocation(Point A, Point B, Point P) {

        double cartProduct = (B.getX() - A.getX()) * (P.getY() - A.getY()) - (B.getY() - A.getY()) * (P.getX() - A.getX());
        if (cartProduct > 0)
            return -1; // Point to the left of AB
        else if (cartProduct == 0)
            return 0;
        else
            return 1; // Point to the right of AB
    }

    /**
     * A helper method to find the perpendicular distance of point P from line AB.
     *
     * @param A
     * @param B
     * @param P
     * @return
     */
    static double perpendicularDist(Point A, Point B, Point P) {
        double ABx = B.getX() - A.getX();
        double ABy = B.getY() - A.getY();
        double num = ABx * (A.getY() - P.getY()) - ABy * (A.getX() - P.getX());
        if (num < 0)
            num = -num;
        return num;
    }

    /**
     * A method to find if a testPoint is inside a triangle ABC.
     *
     * @param testPoint
     * @param A
     * @param B
     * @param C
     * @return
     */
    static boolean isInsideTriangle(Point testPoint, Point A, Point B, Point C) {
        double as_x = testPoint.getX() - A.getX();
        double as_y = testPoint.getY() - A.getY();

        boolean s_ab = (B.getX() - A.getX()) * as_y - (B.getY() - A.getY()) * as_x > 0;

        if ((C.getX() - A.getX()) * as_y - (C.getY() - A.getY()) * as_x > 0 == s_ab)
            return false;

        if ((C.getX() - B.getX()) * (testPoint.getY() - B.getY()) - (C.getY() - B.getY()) * (testPoint.getX() - B.getX()) > 0 != s_ab)
            return false;

        return true;
    }

    /**
     * A method to calculate the the area of a triangle with given vertices A, B and C.
     *
     * @param A
     * @param B
     * @param C
     * @return
     */
    static double triangleArea(Point A, Point B, Point C) {
        double area = (A.getX() * (B.getY() - C.getY()) + B.getX() * (C.getY() - A.getY()) +
                C.getX() * (A.getY() - B.getY())) / 2.0f;
        return Math.abs(area);
    }
}
