package edu.rit.cs;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class CoinMining_Multithreaded {

    /**
     * convert byte[] to hex string
     * @param hash
     * @return hex string
     */
    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    /**
     * get a sha256 of the input string
     * @param inputString
     * @return resulting hash in hex string
     */
    public static String SHA256(String inputString) {
        try {
            MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
            return bytesToHex(sha256.digest(inputString.getBytes(StandardCharsets.UTF_8)));
        }catch (NoSuchAlgorithmException ex) {
            System.err.println(ex.toString());
            return null;
        }
    }

    /**
     * get a randomized target hash
     * @return randomized target hash
     */
    public static String getTargetHash() {
        Random rand = new Random();
        int randInt = rand.nextInt(1000);
        return SHA256(String.valueOf(randInt));
    }

    public static void main(String[] args) {
        String blockHash = SHA256("CSCI-654 Foundations of Parallel Computing");
        System.out.println("BlockHash: " + blockHash);

        String targetHash = "000000038023b712892a41e8438e3ff2242a68747105de0395826f60b38d88dc";
        System.out.println("TargetHash: " + targetHash);

        NonceStatus nonceStatus = new NonceStatus();
        // ToDo: Doesn't work for n_threads = 1.
        int n_cores = Runtime.getRuntime().availableProcessors();
        int n_threads = n_cores;
        int partitionSize = (int)(((long)Integer.MAX_VALUE - (long)Integer.MIN_VALUE)/n_threads);

        Thread[] threads = new Thread[n_threads];
        for(int i=0; i<n_threads; i++){
            int nonce_min = Integer.MIN_VALUE + i*partitionSize, nonce_max;
            if(i == n_threads-1)
                nonce_max = Integer.MAX_VALUE;
            else
                nonce_max = Integer.MIN_VALUE + (i+1)*partitionSize;
            threads[i] = new Thread(new POWThread(i, blockHash, targetHash, nonce_min, nonce_max, nonceStatus));
            threads[i].start();
        }

        long startTime = System.currentTimeMillis();
        for(Thread t:threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Time taken: " + (System.currentTimeMillis()-startTime));

    }

}

class NonceStatus {
    boolean nonceFound = false;
}

class POWThread implements Runnable{

    int nonce_min, nonce_max, threadId;
    String blockHash, targetHash;
    NonceStatus nonceBool;
    POWThread(int threadId, String blockHash, String targetHash, int nonce_min, int nonce_max, NonceStatus nonceBool){
        this.blockHash = blockHash;
        this.targetHash = targetHash;
        this.nonce_min = nonce_min;
        this.nonce_max = nonce_max;
        this.threadId = threadId;
        this.nonceBool = nonceBool;
    }

    @Override
    public void run() {
        //System.out.println("Performing Proof-of-Work...wait...");
        String tmp_hash="undefined";
        for(int nonce=nonce_min; nonce<nonce_max; nonce++) {
            if(nonceBool.nonceFound){
                System.out.println("Nonce already found, terminating Thread "+threadId);
                break;
            }

            tmp_hash = CoinMining_Multithreaded.SHA256(CoinMining_Multithreaded.SHA256(blockHash+String.valueOf(nonce)));
            if(targetHash.compareTo(tmp_hash)>0) {
                nonceBool.nonceFound = true;
                System.out.println("Nonce found by Thread "+threadId+"...");
                System.out.println("Resulting Hash: " + tmp_hash);
                System.out.println("Nonce:" + nonce);
                break;
            }
        }
    }
}