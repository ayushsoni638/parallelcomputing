package edu.rit.cs;

/**
 * A class consisting of code that verifies lemoine's conjecture using a parallel programming approach.
 */
public class AppParallel {

    /**
     * Parallely searches for the largest p satisfying lemoine's conjecture, and the largest n for such p in
     * the range [lowerBound, upperBound].
     *
     * @param lowerBound an odd integer, the lower bound.
     * @param upperBound an odd integer, the upper bound.
     */

    static void parSearch(int lowerBound, int upperBound) {
        int maxP = 0;   // The max value of p to be found
        int maxN = 0;   // The max value of n found corresponding to above p.

        long startTime = System.currentTimeMillis();

        // start parallel section
        // omp parallel
        {
            // Iterate parallely through the odd integers.
            // omp for
            for (int n = lowerBound; n <= upperBound; n += 2) {
                Prime.Iterator pIt = new Prime.Iterator();  // allocate a separate iterator to every thread.
                int p = pIt.next();

                // Keep searching for increasing values of p till these conditions are true:
                // 1: (n-p)/2 is a fraction, or
                // 2: (n-p)/2 is an integer but not a prime.
                while ((n - p) % 2 != 0 || !Prime.isPrime((n - p) / 2)) {
                    p = pIt.next();
                }

                // Update maxP and maxN if this p is larger than existing maxP, omp4j makes sure that maxP, maxN
                // always store largest values even when iterations are not sequential.
                if (p >= maxP) {
                    maxP = p;
                    maxN = n;
                }
            }
        }

        System.out.println("Time taken: " + (System.currentTimeMillis() - startTime) + " milliseconds");
        System.out.println(maxN + " = " + maxP + " + 2*" + (maxN - maxP) / 2);
    }

    public static void main(String[] args) {
        // Note: To avoid confusion, please use "java -classpath ./ edu/rit/cs/AppParallel lowerbound upperbound" for
        // executing after compiling with omp4j, since the package name will cause .class files to go inside edu/rit/cs/.
        // Parse CLI arguments.
        int lowerBound = Integer.parseInt(args[0]), upperBound = Integer.parseInt(args[1]);
        // Perform parallel search.
        parSearch(lowerBound, upperBound);
    }
}
