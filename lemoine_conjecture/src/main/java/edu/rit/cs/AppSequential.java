package edu.rit.cs;

/**
 * A class consisting of code that verifies lemoine's conjecture using a sequential programming approach.
 */

public class AppSequential {

    /**
     * Searches for the largest p satisfying lemoine's conjecture, and the largest n for such p in
     * the range [lowerBound, upperBound].
     *
     * @param lowerBound an odd integer, the lower bound.
     * @param upperBound an odd integer, the upper bound.
     */
    static void seqSearch(int lowerBound, int upperBound) {
        int maxP = 0;   // The max value of p to be found
        int maxN = 0;   // The max value of n found corresponding to above p.
        Prime.Iterator pIt = new Prime.Iterator();

        long startTime = System.currentTimeMillis();

        for (int n = lowerBound; n <= upperBound; n += 2) {
            pIt.restart();  //restart prime iterator for p for every n.
            int p = pIt.next();

            // Keep searching for increasing values of p till these conditions are true:
            // 1: (n-p)/2 is a fraction, or
            // 2: (n-p)/2 is an integer but not a prime.
            while ((n - p) % 2 != 0 || !Prime.isPrime((n - p) / 2)) {
                p = pIt.next();
            }

            // Update maxP and maxN if this p is larger than existing maxP.
            if (p >= maxP) {
                maxP = p;
                maxN = n;
            }
        }

        System.out.println("Time taken: " + (System.currentTimeMillis() - startTime) + " milliseconds");
        System.out.println(maxN + " = " + maxP + " + 2*" + (maxN - maxP) / 2);
    }


    public static void main(String[] args) {

        // Parse CLI arguments.
        int lowerBound = Integer.parseInt(args[0]), upperBound = Integer.parseInt(args[1]);
        // Perform sequential search.
        seqSearch(lowerBound, upperBound);
    }
}
